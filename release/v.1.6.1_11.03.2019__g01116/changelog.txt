# Changelog
All notable changes to releasemaker.py project will be documented in this file.

## v.1.6.1_11.03.2019
### Added
- Add "prefix" param
- Add a colorful output

## v.1.6.0_07.02.2019
### Added
- Add "${date}" and "${version}" placeholders

## v.1.5.1_11.01.2019
### Fixed
- '${release}' placeholder now truly avaliable for 'exe' command

## v.1.5.0_27.11.2018
### Changed
- update help

## Fixed
- create folder while copy/mv if it doesn't exists
- ignore file/folder while rm if it doesn't exists

## v.1.4.0_09.09.2018
### Added
- verbose and work_dir fields to run command

### Changed
- -r to -rv arg
- exec filed to exe (run command)

## Fixed
- problem with printing --help

## v.1.3.0_06.09.2018
### Added
- rm command
- making release branch
- debug flags
- printing all files that should contain the correct version

### Changed
- app description

### Fixed
- more utf8 errors (json, zip, etc)


## v.1.2.2_31.08.2018
### Fixed
- utf8 error while the returning a run pipe error 


## v.1.2.1_31.08.2018
### Fixed
- utf8 error while checking version files


## v.1.2.0_26.07.2018
### Added
- "sequence" filed. This field can be used for a proceeding some steps in a strict order. Each step can be a "copy"ing, "move"ing or "run"ing some stuff.

### Removed
- "paths_to_exec" and "paths_to_other" fields (not needed any more, added "sequence" instead)

### Changed
- '--help' description (added about "sequence")
- changelog update


## v.1.1.0_19.07.2018
### Added
- Removing release folder if an exception accrued
- Add more flexible "version_in_files" and "version_in_files_check_enable" fields instead of "version_h"

### Changed
- '--help' description (added about "version_in_files")


## v.1.0.0_18.07.2018
### Added
- First version of script releasemaker.py.
- Script can get project version from git.
- Script can check a date located in the git version.
- Script can check a version located in a changelog.
- Script can check a version located in version.h files.
- Script can copy exec and other files/folders described in a config into the release folder.
- Script can make archive with release folder. 
- "output_folder", "make_archive" fields in the test config
- Added 'release' folder into this project
 
### Changed
- Changed structure of "version_h" config field


## v.0.0.0_18.07.2018
### Added
- This changelog file
- Short description in README.md
- Folders for a testing script work
- Configuration file template for a testing script work
- Launch file for VS Code