#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utility for a making releases.
   Use --help to get help."""

import subprocess
import datetime
import shutil
import glob
import codecs
import os
from sys import stdin
import argparse
import json

VERSION = "v.1.1.0_19.07.2018"
DESCRIPTION = """Utility for a making releases.

Before start the script you have to:
  1) make release branch with name like v.6.6.6_01.01.2066
     (use actual date and version)
  1) add lines into changelog.txt file,
  2) make commit.

Script should be located in a some Git repository folder.
'release_config.json' should be provided or you can pass
another config json-file throw '--conf' arg into this script.

Config should contain such fields as:
  - "project" is a name of your project,
  - "changelog" is a path to chagelog file,
  - "output_folder" is a folder where release folder
    will be created,
  - "make_archive" is set if an archive will be created,
  - "paths_to_exec" is an array of paths to exec
    files (like '.hex'),
  - "paths_to_other" [optional] is an array of paths
    to other stuff than sould be copied into release
    folder
  - "version_in_files_check_enable" — if enabled then some
    files from "version_in_files" section will be checked on
    correctness of an version (or it's parts) included inside
  - "version_in_files" is an array of paths to some text files
    that will be checked (see abowe)
    - "check_version" [optional, true] — if enabled "v.x.y.z" part
      will be checked
    - "check_date" [optional, false] — if enabled date part
      will be checked
    - "check_commit_hash" [optional, false] — if enabled commit
      hash part will be checked
    - "path" is a path file that will be checked


Here is config example:
{
    "project": "test_project",
    "changelog": "./changelog.txt",
    "output_folder": "./",
    "make_archive": true,
    "paths_to_exec": [
        "./test_folders/test_hex_folder/project.hex",
        "./test_folders/test_hex_folder/project_another.hex"
    ],
    "paths_to_other": [
        "./test_folders/test_random_stuff_folder/random_stuff/",
        "./test_folders/test_random_stuff_folder/stuff.txt"
    ],
    "version_in_files_check_enable": true,
    "version_in_files": [
        {
            "check_version": true,
            "check_date": true,
            "check_commit_hash": false,
            "path": "./test_folders/test_version_folder/version.h"
        },
        {
            "check_version": true,
            "check_date": false,
            "check_commit_hash": true,
            "path": "./test_folders/test_another_version_folder/version.h"
        }
    ]
}
"""


class Error(Exception):
    """Base class for exceptions in this module."""
    def __init__(self, msg, last_err=None):
        if (last_err is None):
            last_err = ""
        else:
            last_err = "\nError \'{}\': {}".format(
                type(last_err).__name__, str(last_err).replace("\n", ""))
        super(Error, self).__init__("{}{}".format(msg, last_err))


class GitNotFoundInPath(Error):
    """Git app was not found in PATH."""
    def __init__(self):
        super(GitNotFoundInPath, self).__init__(
              "Git app was not found in PATH!"
              "Try to add path to python into your env.")


class GitRepoNotFound(Error):
    """Git repository not found."""
    def __init__(self):
        super(GitRepoNotFound, self).__init__(
              "Git repository not found at the script location!\n"
              "You have to start the script only from a some "
              "repository folder.")


class BadConfigFormat(Error):
    """Bad config format."""
    def __init__(self, last_err):
        super(BadConfigFormat, self).__init__(
              "Bad config format! Check your config.json file.", last_err)


class ConfigFieldNotFound(Error):
    """Some config field is not found"""
    def __init__(self, field_name):
        super(ConfigFieldNotFound, self).__init__(
              "Field \"{}\" is required! "
              "Check your config.json file.".format(field_name))


class ConfigFieldHasWrongType(Error):
    """Some config field has wrong type"""
    def __init__(self, field_name):
        super(ConfigFieldHasWrongType, self).__init__(
              "Field \"{}\" has wrong type! "
              "Check your config.json file.".format(field_name))


class VersionOfHexNotEqualToGit(Error):
    """Version contained in hex is not equal to a git version."""
    def __init__(self, hex_name):
        super(VersionOfHexNotEqualToGit, self).__init__(
            "Version contained in {} is not equal to a git version!\n"
            "You have to recompile a project(s)."
            .format(hex_name))


class ChangelogNotChanged(Error):
    """Changelog does not contain a current version changes description."""
    def __init__(self):
        super(ChangelogNotChanged, self).__init__(
            "Changelog does not contain a current "
            "version changes description!")


class ChangelogNotFound(Error):
    """changelog.txt file not found"""
    def __init__(self):
        super(ChangelogNotFound, self).__init__(
            "changelog.txt file not found!")


class PathNotFound(Error):
    """Path provided by config is not found."""
    def __init__(self, last_err):
        super(PathNotFound, self).__init__(
            "Path provided by config is not found!", last_err)


class NotActualDate(Error):
    """Date found in version is not actual."""
    def __init__(self):
        super(NotActualDate, self).__init__(
              "Date found in version is not actual!")


class NotReleaseBranch(Error):
    """HEAD not on release branch."""
    def __init__(self):
        super(NotReleaseBranch, self).__init__(
              "You can not make relese not on release branch!")


class Defaults:
    CHANGELOG_NAME = 'changelog.txt'
    CONFIG_NAME = 'release_config.json'


def _path_leaf(path):
    head, tail = os.path.split(path)
    return tail or os.path.basename(head)


def _get_or_raise(d, key):
    value = d.get(key, None)
    if value is None:
        raise ConfigFieldNotFound(key)
    return value


class _VersionInFiles:
    def __init__(self, init_struct):
        if type(init_struct) is not list:
            raise ConfigFieldHasWrongType("version_in_files")

        self.files = init_struct

        opt_keys = {"check_date": False,
                    "check_commit_hash": False,
                    "check_version": True}

        req_keys = {"path": ""}

        for f in self.files:
            keys = [k for k in f.keys()]
            keys_for_type_check = []

            for k in opt_keys.keys():
                if k not in keys:
                    f[k] = opt_keys[k]
                else:
                    keys_for_type_check.append(k)

            for k in req_keys.keys():
                if k not in keys:
                    raise ConfigFieldNotFound(k)
                else:
                    keys_for_type_check.append(k)

            for k in keys_for_type_check:
                if k in req_keys:
                    if not isinstance(req_keys[k], type(f[k])):
                        raise ConfigFieldHasWrongType(k)
                else:
                    if not isinstance(opt_keys[k], type(f[k])):
                        raise ConfigFieldHasWrongType(k)

    def get_strs_to_check(self, f, version, date, commit_hash):
        local = {"strs": [], "is_split_next": True}

        def add_or_split(str, cond, delimiter=""):
            if (cond):
                if local["is_split_next"]:
                    local["strs"].append(str)
                    local["is_split_next"] = False
                else:
                    local["strs"][-1] += delimiter + str
            else:
                local["is_split_next"] = True

        add_or_split(version, f["check_version"])
        add_or_split(date, f["check_date"], "_")
        add_or_split(commit_hash, f["check_commit_hash"], "__")

        return local["strs"]


class ReleaseMaker:
    def __init__(self, conf_path):
        self.__conf_path = conf_path
        self.__conf = self.__parse_config()

    def __parse_config(self):
        try:
            with open(self.__conf_path, 'r') as conf_file:
                conf = json.load(conf_file)
        except json.decoder.JSONDecodeError as e:
            raise BadConfigFormat(e)

        self.__project_name = _get_or_raise(conf, "project")
        self.__changelog = _get_or_raise(conf, "changelog")
        self.__output_folder = _get_or_raise(conf, "output_folder")
        self.__paths_to_exec = _get_or_raise(conf, "paths_to_exec")
        self.__paths_to_others = conf.get("paths_to_other", [])
        self.__is_make_archive = conf.get("make_archive", True)

        is_ver_h_check_enabled =\
            _get_or_raise(conf, "version_in_files_check_enable")
        if (is_ver_h_check_enabled):
            self.__version_in_files =\
                _VersionInFiles(_get_or_raise(conf, "version_in_files"))
        else:
            self.__version_in_files = None

        return conf

    def __check_git_repo_existing(self):
        try:
            p = subprocess.Popen('git status',
                                 stdout=subprocess.PIPE, shell=True)
            git_responce = p.communicate()[0].decode()
        except OSError as e:
            print(e)
            raise GitNotFoundInPath()

        if git_responce.find("fatal") == 0:
            raise GitRepoNotFound()

    def __get_git_responce(self):
        self.__check_git_repo_existing()
        p = subprocess.Popen('git describe --always --dirty=+'
                             ' --abbrev=5 &&'
                             'git symbolic-ref --short HEAD',
                             stdout=subprocess.PIPE, shell=True)

        git_responce = p.communicate()[0].decode().split('\n')
        git_describe, branch_name = git_responce[:2]
        last_commit_hash = git_describe.split('-')[-1]

        if (branch_name.find("release/") == -1):
            raise NotReleaseBranch()

        version = branch_name.split('/')[-1]

        return version, last_commit_hash

    def __check_string(self, file_path, string):
        with open(file_path, 'r') as version_h:
            f_str = version_h.read().replace('\n', '')

        if f_str.find(string) != -1:
            return True
        else:
            return False

    def __check_program_versions(self, full_version):
        if self.__version_in_files is None:
            return

        version, date, _, commit_hash = full_version.split("_")
        files = self.__version_in_files.files

        for f in files:
            strs = self.__version_in_files.get_strs_to_check(f, version, date,
                                                             commit_hash)
            for s in strs:
                is_str_found = self.__check_string(f["path"], s)
                if not is_str_found:
                    raise VersionOfHexNotEqualToGit(f["path"])

    def __check_changelog(self, version_without_commit_hash):
        try:
            with codecs.open(self.__changelog, encoding='utf-8') as changelog:
                changelog_str = changelog.read().replace('\n', '')
        except FileNotFoundError:
            raise ChangelogNotFound()

        if changelog_str.find(version_without_commit_hash) == -1:
            raise ChangelogNotChanged()

    def __check_date(self, version):
        date = datetime.date.today().strftime('%d.%m.%Y')
        if date != version.split('_')[1]:
            raise NotActualDate()

    def __copy_releases(self, output):
        for release_path in self.__paths_to_exec:
            shutil.copyfile(
                release_path,
                os.path.join(output,
                             _path_leaf(release_path)))

        shutil.copyfile(
            self.__changelog,
            os.path.join(output,
                         _path_leaf(self.__changelog)))

    def __copy_others(self, output):
        for path in self.__paths_to_others:
            if (os.path.isfile(path)):
                shutil.copy(path, os.path.join(output, _path_leaf(path)))
            else:
                shutil.copytree(path,
                                os.path.join(output, _path_leaf(path)))

    def make_release(self):
        (version, commit_hash) = self.__get_git_responce()
        full_release_version = version + "__" + commit_hash
        release_folder_name = os.path.join(self.__output_folder,
                                           full_release_version)

        print('Current release version of'
              ' \"{}\" project is: {}'.format(self.__project_name,
                                              full_release_version))

        try:
            self.__check_date(version)
            self.__check_changelog(version)
            self.__check_program_versions(full_release_version)

            if os.path.exists(release_folder_name):
                shutil.rmtree(release_folder_name, ignore_errors=True)
            os.mkdir(release_folder_name)

            self.__copy_releases(release_folder_name)
            self.__copy_others(release_folder_name)

            if self.__is_make_archive:
                shutil.make_archive(release_folder_name, 'zip',
                                    release_folder_name)

            print('Finished!')
        except IOError as e:
            shutil.rmtree(release_folder_name, ignore_errors=True)
            raise PathNotFound(e)
        except:
            shutil.rmtree(release_folder_name, ignore_errors=True)
            raise

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='releasemaker',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=DESCRIPTION)

    parser.add_argument('--version',
                        action='version',
                        version='%(prog)s {}'.format(VERSION))

    parser.add_argument('--conf',
                        help='path to \'config.json\' file.',
                        dest='conf_path',
                        default=Defaults.CONFIG_NAME)

    args = parser.parse_args()

    try:
        app = ReleaseMaker(args.conf_path)
        app.make_release()
    except Error as e:
        print("\n{}\n\nUse --help to get help.".format(e))
        
